package com.flycms.common.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 13:35 2018/11/13
 */
@Configuration
@ConfigurationProperties(prefix = "site")
@Setter
@Getter
public class SiteConstants {
    /** 上传文件路径**/
    private String uploadLoaderPath;
}
