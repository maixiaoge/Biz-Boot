package com.flycms.filter;
import com.flycms.common.utils.CheckUrlUtils;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Locale;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 扩展FormAuthenticationFilter实现动态改变LoginUrl
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 9:24 2019/8/28
 */
public class CustomFormAuthenticationFilter extends FormAuthenticationFilter {

    /**
     * 重写登录地址
     */
    @Override
    protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        String loginUrl = getLoginUrl();
        String url = req.getRequestURI();
        if (url.contains("/member/")) {
            loginUrl = "/member/login.do";
        }
        WebUtils.issueRedirect(request, response, loginUrl);
    }
}
