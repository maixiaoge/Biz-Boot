package com.flycms.modules.visit.service;

import com.flycms.modules.visit.entity.Visit;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 访问记录服务类
 * @email 79678111@qq.com
 * @Date: 14:35 2019/11/18
 */
public interface VisitService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 添加访问记录
     *
     * @param visit
     * @return
     */
    public Object addVisit(Visit visit);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除访问记录
     *
     * @param id
     * @return
     */
    public int deleteById(Long id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按id查询访问记录
     *
     * @param id
     * @return
     */
    public Visit findById(Long id);
}
