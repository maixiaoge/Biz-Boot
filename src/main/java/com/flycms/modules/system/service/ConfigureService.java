package com.flycms.modules.system.service;

import com.flycms.common.utils.result.Result;
import com.flycms.modules.system.entity.Configure;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 12:55 2019/8/17
 */
public interface ConfigureService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 添加系统配置信息
     *
     * @param system
     * @return
     */
    public Result addSystem(Configure system);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 批量删除配置信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public Result deleteByIds(String ids);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 按id修改系统配置信息
     *
     * @param system 系统配置信息
     * @return 结果
     */
    public Object updateSystem(Configure system);

    /**
     * 按keyCode修改系统配置信息
     *
     * @param keyCode
     * @param keyValue
     * @return 结果
     */
    public Object updateKeyValue(String keyCode, String keyValue);
    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询系统变量keyCode是否有重复
     *
     * @param keyCode
     * @param id 需要排除id
     * @return
     */
    public boolean checkSystemByKeycode(String keyCode, Long id);

    /**
     * 按id查询配置信息
     *
     * @param id
     * @return
     */
    public Configure findById(Long id);

    //按系统变量key查询对应的value
    public String findByKeyCode(String keyCode);

    public Object selectSystemListLayPager(Configure system, Integer page, Integer pageSize, String sort, String order);
}
