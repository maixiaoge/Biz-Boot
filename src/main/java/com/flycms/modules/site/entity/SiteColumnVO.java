package com.flycms.modules.site.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 21:46 2019/10/5
 */
@Setter
@Getter
public class SiteColumnVO  implements Serializable {
    private static final long serialVersionUID = 1L;
    // Fields
    private Long id;
    private Long channel;
    private String columnTitle;
    private String moduleShow;
    private String columnUrl;
    private Integer newWindow;
}
