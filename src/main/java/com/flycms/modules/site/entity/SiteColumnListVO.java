package com.flycms.modules.site.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 网站栏目
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:18 2019/8/17
 */
@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SiteColumnListVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;

	@JsonSerialize(using = ToStringSerializer.class)
	private Long parentId;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long siteId;
	//对应模型表fly_system_module里的id，每个id对应一个模型
	@JsonSerialize(using = ToStringSerializer.class)
	private Long channel;
	private String columnTitle;
	private String columnUrl;
	private Integer newWindow;
	private String columnIcon;	//本栏目的图片、图标，可在模版中使用{siteColumn.icon}进行调用此图以显示
	private Integer columnHidden;	//栏目间的排序
	private Integer columnType;      //栏目属性，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
	@JsonSerialize(using = ToStringSerializer.class)
	private Long tempindex;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long templist;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long tempcontent;
	@JsonSerialize(using = ToStringSerializer.class)
	private Long tempalone;
	private String title;
	private String keywords;
	private String description;
	private String content;
	private Integer sortOrder;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime updateTime;
	private Integer status;
	private Integer deleted;
}