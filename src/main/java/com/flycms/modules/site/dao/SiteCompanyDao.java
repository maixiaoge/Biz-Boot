package com.flycms.modules.site.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.site.entity.SiteCompany;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface SiteCompanyDao extends BaseDao<SiteCompany> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 按网站id删除网站与企业关联信息
     *
     * @param siteIds
     *         网站id
     * @return
     */
    public int deleteSiteCompanyById(Long[] siteIds);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    public int checkSiteCompany(@Param("companyId") Long companyId,@Param("siteId") Long siteId);

    /**
     * 企业id查询当前拥有的网站数量
     *
     * @param companyId
     * @return
     */
    public Set<Long> querySiteIdList(@Param("companyId") Long companyId);
}
