package com.flycms.modules.site.tags;

import com.flycms.common.utils.StringUtils;
import com.flycms.initialize.TagsPlugin;
import com.flycms.modules.site.entity.SiteColumn;
import com.flycms.modules.site.entity.SiteColumnVO;
import com.flycms.modules.site.service.SiteColumnService;
import com.flycms.modules.site.service.SiteService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 功能说明：用于获取栏目列表
 * 适用范围：封面模板、列表模板、文档模板
 * (1)基本语法
 * <@fly_channel row='' type=''>
 * 自定义样式模板(InnerText)
 * </@fly_channel>
 * (2)属性
 * [1] row='数字' 表示获取记录的条数（通用在某级栏目太多的时候使用，默认是 8）
 * [2] type = top,sun/son,self
 * type='top' 表示顶级栏目
 * type='son' 或 'sun' 表示下级栏目
 * type='self' 表示同级栏目
 * 其中后两个属性必须在列表模板中使用。
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Channel extends TagsPlugin {

	@Autowired
	private SiteColumnService siteColumnService;

	@Autowired
	private SiteService siteService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_28);
		HttpServletRequest httpRequest=(HttpServletRequest)request;
		String domainString=StringUtils.twoStageDomain(httpRequest.getRequestURL().toString());
		if (!org.springframework.util.StringUtils.isEmpty(domainString)) {
			long siteId = siteService.findSiteByDomain(domainString);
			if (!org.springframework.util.StringUtils.isEmpty(siteId)) {
				// 获取页面的参数
				Integer row = 8;
				long parentId = 0;
				//处理标签变量
				Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
				for(String str:paramWrap.keySet()){
					if("row".equals(str)){
						row = Integer.parseInt(paramWrap.get(str).toString());
					}
					if("parentId".equals(str)){
						parentId = Long.parseLong(paramWrap.get(str).toString());
					}
				}
				List<SiteColumnVO> channel_list = siteColumnService.selectColumnList(siteId,row,parentId);
				env.setVariable("channel_list", builder.build().wrap(channel_list));
				body.render(env.getOut());
			}
		}
	}
}
