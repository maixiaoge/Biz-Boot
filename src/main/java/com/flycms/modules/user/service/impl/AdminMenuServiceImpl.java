package com.flycms.modules.user.service.impl;

import com.flycms.common.entity.Ztree;
import com.flycms.common.utils.StringUtils;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.parser.TreeParser;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.user.dao.AdminMenuDao;
import com.flycms.modules.user.dao.AdminRoleMenuMergeDao;
import com.flycms.modules.user.entity.AdminMenu;
import com.flycms.modules.user.entity.AdminMenuVO;
import com.flycms.modules.user.entity.AdminRole;
import com.flycms.modules.user.service.AdminMenuService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 菜单 业务层处理
 * 
 * @author 孙开飞
 */
@Service
public class AdminMenuServiceImpl implements AdminMenuService
{
    public static final String PREMISSION_STRING = "perms[\"{0}\"]";

    @Autowired
    private AdminMenuDao adminMenuDao;

    @Autowired
    private AdminRoleMenuMergeDao adminRoleMenuMergeDao;


    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 新增保存菜单信息
     *
     * @param adminMenu 菜单信息
     * @return 结果
     */
    @Override
    @Transactional
    public Object saveMenu(AdminMenu adminMenu)
    {
        if(this.checkMenuNameUnique(adminMenu.getMenuName(),adminMenu.getParentId(),null)){
            return Result.failure("该节点下菜单名称已存在");
        }
        adminMenu.setId(SnowFlakeUtils.nextId());
        adminMenu.setCreateUserId(ShiroUtils.getLoginUser().getId());
        int total = adminMenuDao.save(adminMenu);
        if(total > 0){
            return Result.success("菜单添加成功");
        }else{
            return Result.failure("菜单添加失败");
        }
    }


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 删除菜单管理信息
     *
     * @param id 菜单ID
     * @return 结果
     */
    @Override
    @Transactional
    public Object deleteMenuById(Long id)
    {
        int countMenu=this.selectCountMenuByParentId(id);
        if(countMenu > 0){
            return Result.failure("存在子菜单,不允许删除");
        }
        int total = adminMenuDao.deleteMenuById(id);
        if(total > 0){
            //删除菜单与角色关联信息
            adminRoleMenuMergeDao.selectCountRoleMenuByMenuId(id);
            return Result.success("菜单删除成功");
        }else{
            return Result.failure("菜单删除失败");
        }
    }


    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改保存菜单信息
     *
     * @param adminMenu 菜单信息
     * @return 结果
     */
    @Override
    @Transactional
    public Object updateMenu(AdminMenu adminMenu)
    {
        if(this.checkMenuNameUnique(adminMenu.getMenuName(),adminMenu.getParentId(),adminMenu.getId())){
            return Result.failure("该节点下菜单名称已存在");
        }
        if("M".equals(adminMenu.getMenuType())){
            adminMenu.setUrl("#");
        }
        adminMenu.setUpdateUserId(SnowFlakeUtils.nextId());
        int total = adminMenuDao.updateMenu(adminMenu);
        if(total > 0){
            return Result.success("菜单更新成功");
        }else{
            return Result.failure("菜单更新失败");
        }
    }

    /**
     * 修改菜单是否显示
     *
     * @param visible
     *         菜单状态（1显示 0隐藏）
     * @param id
     *         需要更新的信息id
     * @return
     *         返回成功条数
     */
    @Override
    public Object updateMenuVisible(Boolean visible, Long id)
    {
        int total = adminMenuDao.updateMenuVisible(visible, SnowFlakeUtils.nextId(), id);
        if(total > 0){
            AdminMenu adminMenu=new AdminMenu();
            adminMenu.setVisible(visible);
            //前台更具菜单显示状态修改页面元素
            return Result.success("菜单显示状态更新成功",adminMenu);
        }else{
            return Result.failure("菜单显示状态更新失败");
        }
    }
    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 校验菜单名称是否唯一
     *
     * @param menuName 菜单名称
     * @param parentId 父菜单ID
     * @param id 需要排除的id
     * @return 结果
     */
    @Override
    public boolean checkMenuNameUnique(String menuName, Long parentId, Long id)
    {
        AdminMenu info = adminMenuDao.checkMenuNameUnique(menuName, parentId,id);
        return info != null ? true : false;
    }

    /**
     * 根据菜单ID查询信息
     *
     * @param id 菜单ID
     * @return 菜单信息
     */
    @Override
    public AdminMenu findById(Long id)
    {
        return adminMenuDao.findById(id);
    }

    /**
     * 根据用户查询菜单
     * 
     * @param userId 用户信息
     * @return 菜单列表
     */
    @Override
    public List<AdminMenu> selectMenusByUser(Long userId)
    {
        List<AdminMenu> menus = new LinkedList<AdminMenu>();
        // 管理员显示所有菜单信息
        if (SecurityUtils.getSubject().hasAllRoles(Arrays.asList("admin")))
        {
            menus = adminMenuDao.selectMenuNormalAll();
        }
        else
        {
            menus = adminMenuDao.selectMenusByUserId(userId);
        }
        return getChildPerms(menus, 0);
    }

    /**
     * 查询菜单集合
     * 
     * @return 所有菜单信息
     */
    @Override
    public List<AdminMenu> selectMenuList(AdminMenu adminMenu, Long userId)
    {
        List<AdminMenu> menuList = null;
        if (SecurityUtils.getSubject().hasAllRoles(Arrays.asList("admin")))
        {
            menuList = adminMenuDao.selectMenuList(adminMenu);
        }
        else
        {
            adminMenu.getParams().put("userId", userId);
            menuList = adminMenuDao.selectMenuListByUserId(adminMenu);
        }
        return menuList;
    }

    /**
     * 查询菜单集合
     * 
     * @return 所有菜单信息
     */
    @Override
    public List<AdminMenu> selectMenuAll(Long userId)
    {
        List<AdminMenu> menuList = null;
        if (SecurityUtils.getSubject().hasAllRoles(Arrays.asList("admin"))){
            menuList = adminMenuDao.selectMenuAll();
        }else{
            menuList = adminMenuDao.selectMenuAllByUserId(userId);
        }
        return menuList;
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectPermsByUserId(Long userId)
    {
        List<String> perms = adminMenuDao.selectPermsByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms)
        {
            if (StringUtils.isNotEmpty(perm))
            {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据角色ID查询菜单
     * 
     * @param adminRole 角色对象
     * @return 菜单列表
     */
    @Override
    public List<Ztree> roleMenuTreeData(AdminRole adminRole, Long userId)
    {
        Long roleId = adminRole.getId();
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<AdminMenu> menuList = selectMenuAll(userId);
        if (StringUtils.isNotNull(roleId)){
            List<Long> roleMenuList = adminRoleMenuMergeDao.selectRoleMenuMerge(roleId);
            ztrees = initZtree(menuList, roleMenuList, true);
        } else {
            ztrees = initZtree(menuList, null, true);
        }
        return ztrees;
    }

    /**
     * 查询所有菜单
     * 
     * @return 菜单列表
     */
    @Override
    public List<Ztree> menuTreeData(Long userId)
    {
        List<AdminMenu> menuList = selectMenuAll(userId);
        List<Ztree> ztrees = initZtree(menuList);
        return ztrees;
    }

    /**
     * 查询角色ID查询菜单
     *
     * @return 菜单列表
     */
    @Override
    public List<Ztree> adminRoleMenuTreeData(Long roleId)
    {
        List<Long> roleMenuList = adminRoleMenuMergeDao.selectRoleMenuMerge(roleId);
        List<AdminMenu> menuList = adminMenuDao.selectMenuAll();
        List<Ztree> ztrees = initZtree(menuList, roleMenuList, false);
        return ztrees;
    }

    /**
     * 查询系统所有权限
     * 
     * @return 权限列表
     */
    @Override
    public LinkedHashMap<String, String> selectPermsAll(Long userId)
    {
        LinkedHashMap<String, String> section = new LinkedHashMap<>();
        List<AdminMenu> permissions = selectMenuAll(userId);
        if (StringUtils.isNotEmpty(permissions))
        {
            for (AdminMenu adminMenu : permissions)
            {
                section.put(adminMenu.getUrl(), MessageFormat.format(PREMISSION_STRING, adminMenu.getPerms()));
            }
        }
        return section;
    }

    /**
     * 对象转菜单树
     * 
     * @param menuList 菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<AdminMenu> menuList)
    {
        return initZtree(menuList, null, false);
    }

    /**
     * 对象转菜单树
     * 
     * @param menuList 菜单列表
     * @param roleMenuList 角色已存在菜单列表
     * @param permsFlag 是否需要显示权限标识
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<AdminMenu> menuList, List<Long> roleMenuList, boolean permsFlag)
    {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleMenuList);
        for (AdminMenu menu : menuList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(menu.getId());
            ztree.setPId(menu.getParentId());
            ztree.setName(transMenuName(menu, permsFlag));
            ztree.setTitle(menu.getMenuName());
            if (isCheck)
            {
                ztree.setChecked(roleMenuList.contains(menu.getId()));
            }
            ztrees.add(ztree);
        }
        return ztrees;
    }

    public String transMenuName(AdminMenu adminMenu, boolean permsFlag)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(adminMenu.getMenuName());
        if (permsFlag)
        {
            sb.append("<font color=\"#888\">&nbsp;&nbsp;&nbsp;" + adminMenu.getPerms() + "</font>");
        }
        return sb.toString();
    }

    /**
     * 按父ID查询菜单列表
     *
     * @param parentId 菜单父ID
     * @return 结果
     */
    @Override
    public List<AdminMenu> selectMenuByParentId(Long parentId)
    {
        if(org.springframework.util.StringUtils.isEmpty(parentId)){
            parentId = 0L;
        }
        return adminMenuDao.selectMenuByParentId(parentId);
    }

    /**
     * 查询菜单树
     *
     * @return 菜单树列表
     */
    @Override
    public List<AdminMenuVO> selectMenusTree() {
        List<AdminMenu> menusList = adminMenuDao.selectMenuNormalAll();
        List<AdminMenuVO> volsit = new ArrayList<AdminMenuVO>();
        menusList.forEach(menu -> {
            AdminMenuVO menuVo = new AdminMenuVO();
            menuVo.setId(menu.getId());
            menuVo.setParentId(menu.getParentId());
            menuVo.setMenuName(menu.getMenuName());
            menuVo.setSortOrder(menu.getSortOrder());
            menuVo.setUrl(menu.getUrl());
            menuVo.setMenuType(menu.getMenuType());
            menuVo.setVisible(menu.getVisible());
            menuVo.setPerms(menu.getPerms());
            volsit.add(menuVo);
        });
        List<AdminMenuVO> menus=TreeParser.getTreeList(0L,volsit);
        return menus;
    }

    /**
     * 查询子菜单数量
     * 
     * @param parentId 父级菜单ID
     * @return 结果
     */
    @Override
    public int selectCountMenuByParentId(Long parentId)
    {
        return adminMenuDao.selectCountMenuByParentId(parentId);
    }

    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    @Override
    public int selectCountRoleMenuByMenuId(Long menuId)
    {
        return adminRoleMenuMergeDao.selectCountRoleMenuByMenuId(menuId);
    }

    /**
     * 根据父节点的ID获取所有子节点
     * 
     * @param list 分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<AdminMenu> getChildPerms(List<AdminMenu> list, int parentId)
    {
        List<AdminMenu> returnList = new ArrayList<AdminMenu>();
        for (Iterator<AdminMenu> iterator = list.iterator(); iterator.hasNext();)
        {
            AdminMenu t = (AdminMenu) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId)
            {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     * 
     * @param list
     * @param t
     */
    private void recursionFn(List<AdminMenu> list, AdminMenu t)
    {
        // 得到子节点列表
        List<AdminMenu> childList = getChildList(list, t);
        t.setChildren(childList);
        for (AdminMenu tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                // 判断是否有子节点
                Iterator<AdminMenu> it = childList.iterator();
                while (it.hasNext())
                {
                    AdminMenu n = (AdminMenu) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<AdminMenu> getChildList(List<AdminMenu> list, AdminMenu t)
    {
        List<AdminMenu> tlist = new ArrayList<AdminMenu>();
        Iterator<AdminMenu> it = list.iterator();
        while (it.hasNext())
        {
            AdminMenu n = (AdminMenu) it.next();
            if (n.getParentId().longValue() == t.getId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<AdminMenu> list, AdminMenu t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }
}
