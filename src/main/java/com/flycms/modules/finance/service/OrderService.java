package com.flycms.modules.finance.service;

import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:10 2019/8/17
 */
public interface OrderService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询该企业指定产品类型的产品订单id列表
     *
     * @param companyId
     *         企业id
     * @param marketType
     *        产品类型
     * @return
     */
    public Set<Long> queryOrderByProductIdList(Long companyId, Integer marketType);

    /**
     * 查询订单列表
     *
     * @param siteid
     *         所输网站id
     * @param orderSn
     *         订单号
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Object queryOrderPager(Long siteid, String orderSn, Integer page, Integer pageSize, String sort, String order);
}
